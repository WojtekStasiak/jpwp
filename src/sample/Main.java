package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static User actualUser;
    public static User getActualUser() {
        return actualUser;
    }
    public static void setActualUser(User actualUser) {
        Main.actualUser = actualUser;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /* Method created to load next scene. You have to create Parent object, Action Event and send it to method*/
    public static void loadNextScene(Parent parent, ActionEvent actionEvent){
        Scene scene = new Scene(parent);
        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public static void authenticate(String user, String password){
        if(user.equals("") && password.equals("")) {
            /*Display alert - sth went wrong*/
            System.exit(-1);
        }
        else{
            /*Display alert - user logged in*/
        }
    }

    public static void main(String[] args) {
        launch(args);
    }





}
